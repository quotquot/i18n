/*
 * Copyright 2020-2021 Johnny Accot
 *
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at:
 *
 *      http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import i18n from "i18n";
import logging from "logging";
import sanitizer from "sanitizer";

const logger = logging.getLogger("@quotquot/i18n");

export async function localizeElement(el, language, context) {
    const keys = el.dataset.i18n;
    const options = el.dataset.i18nOptions;
    const params = Object.assign({}, context, JSON.parse(options || "{}"));
    const nodes = [];

    const t = await i18n.getFixedT(language);

    // https://github.com/i18next/jquery-i18next/blob/master/README.md
    for (let key of keys.split(";")) {

        key = key.trim();

        if (key.length === 0)
            continue;

        if (key[0] === "[") {
            const parts = key.split("]");
            key = parts[0].slice(1);
            const translation = t(parts[1], params);
            if (key === "html") {
                el.innerHTML = sanitizer.sanitize(translation);
                nodes.push(...el.children);
            }
            else if (key === "append") {
                const lastChild = el.lastChild;
                if (lastChild && lastChild.nodeType === Node.TEXT_NODE)
                    el.removeChild(lastChild);
                const textNode = document.createTextNode(translation);
                el.appendChild(textNode);
                nodes.push(textNode);
            }
            else {
                const attrNode = document.createAttribute(key);
                attrNode.value = translation;
                el.setAttributeNode(attrNode);
                nodes.push(attrNode);
            }
        }
        else {
            if (el.firstChild && el.firstChild.nodeType === Node.TEXT_NODE)
                el.removeChild(el.firstChild);
            const textNode = document.createTextNode(t(key, params));
            el.insertBefore(textNode, el.firstChild);
            nodes.push(textNode);
        }
    }
    return nodes;
}

class Handler {

    constructor(el) {
        this.el = el;
        this.nodes = [];
    }

    async render(component, values, changed) {
        let needsRefresh = false;
        if (!changed) /* full update */
            needsRefresh = true;
        else {
            const dependencies = this.el.dataset.i18nDependencies;
            if (dependencies) {
                const vars = new Set(dependencies.split());
                needsRefresh = changed.some(name => vars.has(name));
                if (needsRefresh)
                    logger.debug(`deps changed for ${this.el.dataset.i18n}:`, vars);
            }
        }
        if (needsRefresh) {
            this.clear();
            this.nodes = await localizeElement(this.el, values.language, values);
        }
    }

    clear() {
        for (const node of this.nodes) {
            if (node.nodeType === Node.ATTRIBUTE_NODE)
                node.ownerElement.removeAttributeNode(node);
            else
                node.remove();
        }
        this.nodes.length = 0;
    }
}


const DATA_I18N = "data-i18n";

export default class I18nPlugin {

    constructor(component) {
        this.handlers = new Map();
        this.update = language => {
            component.update({ language })
                .catch(exc => component.onerror(exc, "plugin", "i18n", "update"));
        };
        component.renderers.push(this);
    }

    async _addHandler(el, component, values) {
        const handler = new Handler(el);
        this.handlers.set(el, handler);
        await handler.render(component, values);
    }

    async _processMutations(component, mutations) {

        /* because the mutation array can be in any order, we need to split
         * it into the main data-i18n attributes and auxiliary ones, process
         * the main list first, and then only the auxiliary one */
        const main = [];
        const aux = [];
        for (const mutation of mutations || this.observer.takeRecords())
            (mutation.attributeName === DATA_I18N ? main : aux).push(mutation);

        /* render the elements with a mutated data-i18n attribute */
        const renderedElements = new Set();
        for (const mutation of main) {
            const el = mutation.target;
            const attr = el.getAttributeNode(DATA_I18N);
            const handler = this.handlers.get(el);
            if (mutation.oldValue === null) {
                /* the data-i18n attribute was added */
                if (!attr)
                    logger.warn("unexpected: element should have data-i18n");
                if (handler)
                    logger.warn("unexpected: handler should not exist");
                await this._addHandler(el, component, component.state);
                renderedElements.add(el);
            }
            else {
                /* the data-i18n attribute existed */
                if (attr) {
                    /* strangely enough, it is possible to receive a mutation
                     * record even though the value did not change */
                    if (mutation.oldValue !== attr.value) {
                        /* the attribute was changed */
                        await handler.render(component, component.state);
                        renderedElements.add(el);
                    }
                }
                else {
                    /* the attribute was removed */
                    handler.clear();
                    this.handlers.delete(el);
                }
            }
        }

        /* now process auxiliary attributes */
        for (const mutation of aux) {

            const el = mutation.target;
            const attrName = mutation.attributeName;

            /* if the element is not in handlers at this point, it has no
             * data-i18n attribute, which means the auxiliary attributes are
             * useless and can be ignored */
            if (!this.handlers.has(el)) {
                if (el.hasAttribute(DATA_I18N))
                    logger.warn("unexpected: no handler for i18n element");
                logger.warn(`unused ${attrName} attribute`);
                continue;
            }

            /* similarly, the element may have been rendered already in the
             * previous loop or when processing another auxiliary attribute;
             * if so, there is nothing more to do */
            if (renderedElements.has(el))
                continue;

            /* at this point, the element has a data-i18n attribute that did
             * not change, but auxiliary attributes that did */
            const handler = this.handlers.get(el);
            await handler.render(component, component.state);
            renderedElements.add(el);
        }
    }

    async setup(component) {
        component.state.language = await i18n.getLanguage();
        i18n.subscribe(this.update);
        this.observer = new MutationObserver(
            mutations => this._processMutations(component, mutations)
                .catch(exc => component.onerror(exc, "plugin", "i18n", "mutation"))
        );
        this.observer.observe(component.shadowRoot, {
            subtree: true,
            attributeFilter: [DATA_I18N, `${DATA_I18N}-options`],
            attributeOldValue: true
        });
    }

    async cleanup(component) {
        if (this.observer) {
            this.observer.disconnect();
            this.observer = null;
        }
        for (const handler of this.handlers.values())
            handler.clear();
        this.handlers.clear();
        i18n.unsubscribe(this.update);
    }

    async renderElement(el, component, values, changed, context, parent) {

        // TODO: the following code does not work is `language` is deleted from
        // the component state

        /* if there is no defined language or the element has no data-i18n
         * attribute, there is nothing to do */
        if (!values.language || !el.hasAttribute(DATA_I18N))
            return;

        const handler = this.handlers.get(el);

        if (handler)
            await handler.render(component, values, changed);
        else
            await this._addHandler(el, component, values, changed);
    }
}

/*
// test code to understand what MutationObserver really does; remove later
const observer = new MutationObserver(mutations => {
console.log(JSON.stringify(mutations.map(mutation => {
const { target, attributeName, oldValue } = mutation;
return {tag:target.tagName, attr:attributeName, new:target.getAttribute(attributeName), old:oldValue};
})));
});
observer.observe(temp1, { subtree: true, attributeOldValue: true });
*/
