import { terser } from "rollup-plugin-terser";

export default {
    input: "index.js",
    output: {
        file: "dist/index.mjs",
        format: "esm"
    },
    plugins: [
        process.env.BUILD === "production" && terser()
    ],
    external: [
        "i18n",
        "logging",
        "sanitizer"
    ]
};
