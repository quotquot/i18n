export default {
    getFixedT: async lang => (key, opt) => `${lang}/${key}/${JSON.stringify(opt)}`,
    getLanguage: async() => "en-US",
    subscribe: cb => {},
    unsubscribe: cb => {}
};
