/* eslint require-jsdoc: off */
/* eslint func-style: off */
/* eslint no-underscore-dangle: off */

import QuotquotElement from "../node_modules/@quotquot/element/dist/index.mjs";
import I18nPlugin from "../index";

const template = document.createElement("template");
template.innerHTML = '<div data-i18n="key"></div>';

class I18nQuotquotElement extends QuotquotElement {
    constructor() {
        super({ Plugins: [I18nPlugin] });
    }
}

window.customElements.define("i18n-test", I18nQuotquotElement);

const el = document.createElement("i18n-test");
document.body.appendChild(el);

test("Create i18n-test element", async() => {
    expect(el.tagName).toBe("I18N-TEST");

    await el.appendTemplate(template);
    expect(el.shadowRoot.innerHTML).toBe('<div data-i18n="key">en-US/key/{"language":"en-US"}</div>');
    const div = el.shadowRoot.firstChild;

    div.setAttribute("data-i18n", "key2");
    await el.plugins[0]._processMutations(el);
    expect(el.shadowRoot.innerHTML).toBe('<div data-i18n="key2">en-US/key2/{"language":"en-US"}</div>');


    div.removeAttribute("data-i18n");
    // TODO: check that attr.handler is removed and the element cleared. Calling either:
    // expect(attr.handler).toBeFalsy();
    // expect(el.shadowRoot.innerHTML).toBe("<div></div>");
    // fails because the mutation observer has not been run yet.
});
